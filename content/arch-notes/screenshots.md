---
title: "Screenshots"
date: 2020-04-14T20:48:38+02:00
toc: true
summary: "Contents of this post: Screenshots of my installed system and programs."
draft: false
tags:
  - vifm
  - spotify
  - unixporn
  - desktop
---

## Vifm filemanager
{{< image src="/img/screenshots/vifm.png" alt="Where" position="center" style="border-radius: 4px;" >}}
\
{{< image src="/img/screenshots/2020-07-14-200430_1920x1080_scrot.png" alt="vifm" position="center" style="border-radius: 4px;" >}}

## Spotify
{{< image src="/img/screenshots/2020-07-14-200641_1920x1080_scrot.png" alt="is" position="center" style="border-radius: 4px;" >}}
\
{{< image src="/img/screenshots/2020-07-14-200702_1920x1080_scrot.png" alt="my" position="center" style="border-radius: 4px;" >}}
\
{{< image src="/img/screenshots/spotify-2.png" alt="mind" position="center" style="border-radius: 4px;" >}}

## Random windows to look cool
{{< image src="/img/screenshots/2020-07-14-204050_1920x1080_scrot.png" alt="?" position="center" style="border-radius: 4px;" >}}
\
{{< image src="/img/screenshots/random-2.png" alt="random" position="center" style="border-radius: 4px;" >}}

## Empty desktop
{{< image src="/img/screenshots/2020-07-14-204120_1920x1080_scrot.png" alt="empty" position="center" style="border-radius: 4px;" >}}
\
{{< image src="/img/screenshots/empty-2.png" alt="empty" position="center" style="border-radius: 4px;" >}}

## One for r/unixporn
{{< image src="/img/screenshots/unixporn.png" alt="unixporn" position="center" style="border-radius: 4px;" >}}
